#include <iostream>
#include <string>
using namespace std;

int main()
{
    int nb;
    double result;
    double a;

    cout << "Veuillez rentrer un nombre" << endl;
    cin >> nb;

    cout << "La table de " << nb << " est :\n" << endl;

    for (a = 0; a <= 10; a++)
    {
        result = nb * a;
        cout << result << endl;
    }
}
